import ResponsiveTable from './ResponsiveTable';
import Form from './pages/Form';

import './App.css';

function App() {
  return (
    <div className="roboto-bold">
      <header className="App-header">
        <h2 className='text-3xl bg-slate-900 rounded-3xl text-white'>Hello TailwindCSS</h2>
      </header>

      <Form />
      <ResponsiveTable />

      {/* <div className="overflow-x-auto">
        <table className="table-auto w-full border-collapse border border-gray-800">
          <thead>
            <tr>
              <th className="px-4 py-2 bg-gray-200 text-gray-800 border border-gray-600">Name</th>
              <th className="px-4 py-2 bg-gray-200 text-gray-800 border border-gray-600">Age</th>
              <th className="px-4 py-2 bg-gray-200 text-gray-800 border border-gray-600">Country</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className="px-4 py-2 border border-gray-600">John Doe</td>
              <td className="px-4 py-2 border border-gray-600">30</td>
              <td className="px-4 py-2 border border-gray-600">USA</td>
            </tr>
            <tr>
              <td className="px-4 py-2 border border-gray-600">Jane Smith</td>
              <td className="px-4 py-2 border border-gray-600">25</td>
              <td className="px-4 py-2 border border-gray-600">Canada</td>
            </tr>
            <tr>
              <td className="px-4 py-2 border border-gray-600">Michael Johnson</td>
              <td className="px-4 py-2 border border-gray-600">35</td>
              <td className="px-4 py-2 border border-gray-600">UK</td>
            </tr>
          </tbody>
        </table>
      </div>  */}


    </div>
  );
}

export default App;
